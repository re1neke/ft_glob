# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/07/02 22:27:03 by mvladymy          #+#    #+#              #
#    Updated: 2019/07/03 00:44:50 by mvladymy         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

LIB_NAME:=libft_glob.a
LIB_SRC:=ft_glob.c dir_match.c asterisk_match.c range_match.c create_pathv.c \
	parse_dirs.c range_match.c sort_paths.c plist.c

TEST_NAME=ft_glob_tests
TEST_SRC:=tests.c

LFT_PATH=libft

CFLAGS:=-Wall -Wextra -Werror -Wconversion -Iinclude -L. \
	-I$(LFT_PATH)/include -L$(LFT_PATH)

LIBFLAGS:=-l:$(LIB_NAME) -lft

all: lib tests

lib: $(LIB_SRC:.c=.o)
	ar rc $(LIB_NAME) $^

tests: $(LIB_NAME) $(TEST_SRC:.c=.o)
	$(CC) $(CFLAGS) $(TEST_SRC:.c=.o)  $(LIBFLAGS) -o $(TEST_NAME)

%.o: %.c include/ft_glob.h
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm -rf $(LIB_SRC:.c=.o) $(TEST_SRC:.c=.o)

fclean: clean	
	rm -rf $(LIB_NAME) $(TEST_NAME)

.NOTPARALLEL: re
re: clean all
